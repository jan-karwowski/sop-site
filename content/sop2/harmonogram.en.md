---
title: "Schedule"
date: 2023-02-14T10:00:00+01:00
---

Summer semester 2022/2023

## Lecture

Wednesdays, 8:15-10:00, room 107

| Date  | Topic                                                                                                 |
|-------|-------------------------------------------------------------------------------------------------------|
| 22.02 | [Lec 1 	Introduction to interprocess communication (IPC). IPC with pipes/FIFOs]({{< ref "wyk/w1" >}}) |
| 01.03 | [Lec 2 	POSIX IPC: shared memory and queues]({{< ref "wyk/w2" >}})                                    |
| 08.03 | [Lec 3 	Synchronization. Deadlocks.]({{< ref "wyk/w3" >}})                                            |
| 15.03 | [Lec 4 	POSIX synchronization]({{< ref "wyk/w4" >}})                                                  |
| 22.03 | [Lec 5 	Introduction to networking]({{< ref "wyk/w5" >}})                                             |
| 29.03 | [Lec 6 	Sockets interface]({{< ref "wyk/w6" >}})                                                      |
| 05.04 | [Lec 7 	Network programming]({{< ref "wyk/w7" >}})                                                    |
| 12.04 | [T1/1 	Test 1.]({{< ref "wyk/k1" >}}), [Lec8. Networking security issues]({{< ref "wyk/w8" >}})       |
| 19.04 | [Lec 9 	Main memory management]({{< ref "wyk/w9" >}})                                                 |
| 26.04 | [Lec 10 	Virtual memory]({{< ref "wyk/w10" >}})                                                       |
| 10.05 | [Lec 11 	File systems]({{< ref "wyk/w11" >}})                                                         |
| 17.05 | [Lec 12 	Implementations of file systems, Distributed file systems.]({{< ref "wyk/w12" >}})           |
| 24.05 | [Lec 13 	Real-time systems.]({{< ref "wyk/w13" >}})                                                   |
| 31.05 | [T2/1 	Test 2]({{< ref "wyk/k2" >}}),  [Lec14. Protection and security.]({{< ref "wyk/w14" >}})       |
| 14.06 | **Lecture Tests Retakes**                                                                             |

## Laboratories

Thursdays, 14:15 - 17:00

| Date  | Topic                                                                        | USOS Group       |
|-------|------------------------------------------------------------------------------|------------------|
| 23.03 | [Lab1 FIFO/pipe]({{< ref "lab/l1">}})                                        | LAB1, LAB3       |
| 30.03 | [Lab1 FIFO/pipe]({{< ref "lab/l1">}})                                        | LAB2, LAB4, LAB5 |
| 13.04 | [Lab2 POSIX queues]({{< ref "lab/l2">}})                                     | LAB1, LAB3       |
| 20.04 | [Lab2 POSIX queues]({{< ref "lab/l2">}})                                     | LAB2, LAB4, LAB5 |
| 27.04 | [Lab3 Network sockets, pselect]({{< ref "lab/l3">}})                         | LAB1, LAB3       |
| 11.05 | [Lab3 Network sockets, pselect]({{< ref "lab/l3">}})                         | LAB2, LAB4, LAB5 |
| 18.05 | [Lab4 Sockets, threads, shared memory, synchronization]({{< ref "lab/l4">}}) | LAB1, LAB3       |
| 25.05 | [Lab4 Sockets, threads, shared memory, synchronization]({{< ref "lab/l4">}}) | LAB2, LAB4, LAB5 |
| 26.05 | 5:00pm: retake signup closes (in moodle)                                     | Everyone         |
| 01.06 | Lab5: Retakes                                                                | Everyone         |
| 15.06 | Lab5: Retakes                                                                | Everyone         |
