---
title: "Harmonogram"
date: 2023-02-14T10:00:00+01:00
---

## Wykłady

Czwartki, g. 8:15 - 10:00, s. 107

| Data  | Tytuł                                                                                                             |
|-------|-------------------------------------------------------------------------------------------------------------------|
| 23.02 | [W1 Wprowadzenie do komunikacji międzyprocesowej (IPC). Łącza.]({{< ref "wyk/w1" >}})                             |
| 02.03 | [W2 Komunikacja międzyprocesowa POSIX: pamięć wspólna i kolejki komunikatów]({{< ref "wyk/w2" >}})                |
| 09.03 | [W3 Synchronizacja. Zakleszczenia.]({{< ref "wyk/w3" >}})                                                         |
| 16.03 | [W4 Synchronizacja POSIX.]({{< ref "wyk/w4" >}})                                                                  |
| 23.03 | [W5 Wprowadzenie do sieci TCP/IP.]({{< ref "wyk/w5" >}})                                                          |
| 30.03 | [W6 Interfejs gniazd.]({{< ref "wyk/w6" >}})                                                                      |
| 06.04 | [W7 Programowanie sieciowe.]({{< ref "wyk/w7" >}})                                                                |
| 13.04 | [K1/1 **Kolokwium 1.**]({{< ref "wyk/k1" >}}), [W8. Zagadnienia bezpieczeństwa sieciowego.]({{< ref "wyk/w8" >}}) |
| 20.04 | [W9 Zarządzanie pamięcią operacyjną.]({{< ref "wyk/w9" >}})                                                       |
| 27.04 | [W10 Pamięć wirtualna.]({{< ref "wyk/w10" >}})                                                                    |
| 11.05 | [W11 Systemy plików.]({{< ref "wyk/w11" >}})                                                                      |
| 18.05 | [W12 Realizacje systemów plików.]({{< ref "wyk/w12" >}}), Rozproszone systemy plików.                             |
| 25.05 | [W13 Systemy czasu rzeczywistego.]({{< ref "wyk/w13" >}})                                                         |
| 01.06 | [K2/1 **Kolokwium 2.**]({{< ref "wyk/k2" >}}), [W14. Ochrona i bezpieczeństwo.]({{< ref "wyk/w14" >}})            |
| 15.06 | **Poprawy kolokwiów**                                                                                             |

## Laboratorium

Poniedziałki, g. 14:15 - 17:00

| Data  | Tytuł                                                                     | Grupy USOS       |
|-------|---------------------------------------------------------------------------|------------------|
| 20.03 | [L1 FIFO/pipe]({{< ref "lab/l1" >}})                                      | LAB1, LAB3, LAB5 |
| 27.03 | [L1 FIFO/pipe]({{< ref "lab/l1" >}})                                      | LAB2, LAB4, LAB6 |
| 03.04 | [L2 Kolejki POSIX]({{< ref "lab/l2" >}})                                  | LAB1, LAB3, LAB5 |
| 17.04 | [L2 Kolejki POSIX]({{< ref "lab/l2" >}})                                  | LAB2, LAB4, LAB6 |
| 08.05 | [L3 Gniazda pselect]({{< ref "lab/l3" >}})                                | LAB1, LAB3, LAB5 |
| 15.05 | [L3 Gniazda pselect]({{< ref "lab/l3" >}})                                | LAB2, LAB4, LAB6 |
| 22.05 | [L4 Gniazda, wątki, pamięć wspólna, synchronizacja]({{< ref "lab/l4" >}}) | LAB1, LAB3, LAB5 |
| 29.05 | [L4 Gniazda, wątki, pamięć wspólna, synchronizacja]({{< ref "lab/l4" >}}) | LAB2, LAB4, LAB6 |
| 30.05 | 17:00 -- zamknięcie zapisów na poprawy w moodle                           | Wszystkie grupy  |
| 05.06 | L5 Poprawy                                                                | Wszystkie grupy  |
| 12.06 | L5 Poprawy                                                                | Wszystkie grupy  |
