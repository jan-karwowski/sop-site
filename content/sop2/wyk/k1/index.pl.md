---
title: "Kolokwium 1"
date: 2022-02-05T16:04:12+01:00
weight: 75
---

- SOP2.W1: [Wprowadzenie do komunikacji międzyprocesowej (IPC). Łącza.]({{<ref "../w1">}})
- SOP2.W2: [Komunikacja międzyprocesowa POSIX: pamięć wspólna i kolejki komunikatów.]({{<ref "../w2">}})
- SOP2.W3: [Synchronizacja. Zakleszczenia.]({{<ref "../w3">}})
- SOP2.W4: [Synchronizacja POSIX.]({{<ref "../w4">}})
- SOP2.W5: [Wprowadzenie do sieci TCP/IP.]({{<ref "../w5">}})
- SOP2.W6: [Interfejs gniazd.]({{<ref "../w6">}})
- SOP2.W7: [Programowanie sieciowe.]({{<ref "../w7">}})
