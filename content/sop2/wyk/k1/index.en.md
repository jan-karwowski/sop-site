---
title: "Lecture Test 1"
date: 2022-02-05T16:20:57+01:00
weight: 75
---

- OPS2.Lec1: [Introduction to interprocess communication (IPC). IPC with pipes/FIFOs.]({{< ref "../w1" >}})
- OPS2.Lec2: [POSIX IPC: shared memory and message queues]({{< ref "../w2" >}})
- OPS2.Lec3: [Synchronization. Deadlocks.]({{< ref "../w3" >}})
- OPS2.Lec4: [POSIX synchronization.]({{< ref "../w4" >}})
- OPS2.Lec5: [Introduction to networking.]({{< ref "../w5" >}})
- OPS2.Lec6: [Sockets interface.]({{< ref "../w6" >}})
- OPS2.Lec7: [Network programming.]({{< ref "../w7" >}})
