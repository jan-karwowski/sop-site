---
title: "Ochrona i bezpieczeństwo"
date: 2022-02-05T15:59:32+01:00
weight: 140
---

# Wykład 14 - Ochrona i bezpieczeństwo

## Zakres wykładu

  - Ochrona
      - Cele ochrony
      - Domeny ochrony
      - Macierz dostępów
      - Implementacja macierzy dostępów
      - Cofanie praw dostępu
      - Systemy działające na zasadzie uprawnień
      - Ochrona na poziomie języka programowania
  - Bezpieczeństwo
      - Problem bezpieczeństwa
      - Standardy bezpieczeństwa
  - Ochrona i bezpieczeństwo w systemach Linux i MSwin

## Materiały

1.  Slajdy: [Ochrona.pdf]({{< resource "Ochrona_3.pdf" >}})
2.  Podręcznik: rozdz. 18: "Ochrona", rozdz. 19: "Bezpieczeństwo", podrozdz. 22.3.7: "Monitor bezpieczeństwa odwołań", 22.4.6 "Podsystemy rejestracji i bezpieczeństwa", 22.5.3 "Bezpieczeństwo".
