---
title: "Protection and security"
date: 2022-02-05T16:19:23+01:00
weight: 140
---

# Lecture 14 - Protection and security

## Scope of the lecture

  - Protection
      - Goals of protection
      - Protection domain
      - Access matrix
      - Access matrix implementations
      - Revocation of access rights
      - Language-based protection
  - Security
      - The security problem
      - Computer security classifications
  - Protection and security in Linux and MSWin

## Reference

1.  Slides: [Prot\_en.pdf]({{< resource "Prot_en_3.pdf" >}})
2.  Textbook: chapter 14: "System protection".  
    Chapter 15: "System security", sections 15.1: "Goal of protection", 15.8: "Computer-security classifications".  
    Chapter 21: "The Linux system", sections 21.11: "Security".  
    Chapter 22: "Windows XP", sections 22.3.3.7 "Security reference monitor", 22.4.6 "Logon and security subsystems", 22.5.3 "Security"
