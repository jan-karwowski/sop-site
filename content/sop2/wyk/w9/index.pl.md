---
title: "Zarządzanie pamięcią"
date: 2022-02-04T22:05:53+01:00
weight: 90
---

# Wykład 9 - Zarządzanie pamięcią operacyjną

## Zakres wykładu

  - Wiązanie adresów logicznych z adresami pamięci operacyjnej:
      - kompilacja
      - konsolidacja: statyczna, dynamiczna, dynamiczna w czasie wykonywania
      - ładowanie absolutne, na żądanie, dynamiczne
      - wykonywanie; nakładkowanie; wymiana.
  - Logiczna i fizyczna przestrzeń adresowania. Jednostka zarządzania pamięcią (MMU).
  - Techniki przydziału pamięci:
      - Ciągły przydział pamięci operacyjnej, dynamiczny przydział
      - Segmentacja: architektura, własności
      - Stronicowanie: architektura, własności. Realizacja tablicy stron, rejestry asocjacyjne (TLB) a efektywny czas dostępu do pamięci. Stronicowanie hierarchiczne. Stronicowanie z tablicą rzadką. Odwrócona tablica stron.
  - Fragmentacja: wewnętrzna i zewnętrzna.
  - Współdzielenie pamięci przy wykorzystaniu segmentacji i stronicowania

## Materiały

1.  Podręcznik: rozdz. 9: "Zarządzanie pamięcią"
2.  Slajdy: [Mem.pdf]({{< resource "Mem_1.pdf" >}})
