---
title: "Bezpieczeństwo sieciowe"
date: 2022-02-04T22:00:19+01:00
weight: 80
---

# Wykład 8 - Zagadnienia bezpieczeństwa sieciowego

## Zakres wykładu

  - Elementarne techniki kryptograficzne.
      - Szyfrowanie:
          - Szyfrowanie symetryczne
          - Szyfrowanie asymetryczne
      - Wyznaczanie kryptograficznego skrótu wiadomości
      - Generacja kryptograficznie bezpiecznych liczb losowych
  - Realizacja usług ochrony informacji:
      - poufność
      - integralność
      - uwierzytelnianie nadawcy
      - niezaprzeczalność
  - Zasada Kerckhoffa
  - Uwierzytelnianie kluczy publicznych:
      - Rozproszona sieć zaufania
      - Scentralizowany system uwierzytelniania
  - Kryptografia w protokołach sieciowych: IPSec, SSL/TLS. Bezpieczne tunelowanie z SSL.

## Materiały

1.  Podręcznik, rozdz. 19.7: "Kryptografia"
2.  Slajdy: [Inet\_4.pdf]({{< resource "Inet_4_3.pdf" >}})
