---
title: "Network security"
date: 2022-02-04T22:00:23+01:00
weight: 80
---

# Lecture 8 - Network security issues

## Scope of the lecture

  - Elementary cryptographic techniques
      - Encryption/decryption:
          - Symmetric cryptography
          - Asymmetric cryptography
      - Computation of message digest
      - Generation of cryptographically secure random numbers
  - Cryptographic services:
      - confidentiality
      - data integrity
      - sender authentication
      - Sender non-repudiation
  - Kerckhoffs' principle
  - Validation of public keys:
      - Distributed web of trust
      - Public Key Infrastructure (PKI)
  - Cryptography in network protocols:IPSec, SSL/TLS. SSL and secure tunneling.

## Reference

1.  Slides: [Inet\_4en.pdf]({{< resource "Inet_4en_1.pdf" >}})
2.  Textbook: chapter 15.4 -- "Cryptography as a security tool"
