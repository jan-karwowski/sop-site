---
title: "Systemy operacyjne 2"
date: 2022-02-10T10:00:00+01:00
---

[Strona przedmiotu Systemy operacyjne 2 w USOSie](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1120-IN000-ISP-0245)

## Obsada

### Wykład

- Paweł Sobótka
  - konsultacje: czwartek 10:00-11:00, pok 524, po uprzednim umówieniu
  - email: pawel.sobotka@pw.edu.pl

### Laboratorium

- *Koordynator laboratorium:* Jan Karwowski
  - email: jan.karwowski@pw.edu.pl
  - uwaga: zwykle nie czytam wiadomości na teamsach
- Gr 1, gr 2:
- Gr 3, gr 4: 
- Gr 5, gr 6: 

