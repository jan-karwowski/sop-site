---
title: "Grading rules"
date: 2022-02-10T10:00:00+01:00
---

The rules are in [usos page describing the course](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1120-IN000-ISA-0244)  after logging in.

See the "Item regulations" field.
