---
title: "Materiały do przedmiotu"
date: 2022-02-10T10:00:00+01:00
---

## Materiały wykładowe

- [W1 Wprowadzenie do komunikacji międzyprocesowej (IPC). Łącza.]({{< ref "wyk/w1" >}})
- [W2 Komunikacja międzyprocesowa POSIX: pamięć wspólna i kolejki komunikatów]({{< ref "wyk/w2" >}})
- [W3 Synchronizacja. Zakleszczenia.]({{< ref "wyk/w3" >}})
- [W4 Synchronizacja POSIX.]({{< ref "wyk/w4" >}})
- [W5 Wprowadzenie do sieci TCP/IP.]({{< ref "wyk/w5" >}})
- [W6 Interfejs gniazd.]({{< ref "wyk/w6" >}})
- [W7 Programowanie sieciowe.]({{< ref "wyk/w7" >}})
- [W8 Zagadnienia bezpieczeństwa.sieciowego.]({{< ref "wyk/w8" >}})
- [W9 Zarządzanie pamięcią operacyjną.]({{< ref "wyk/w9" >}})
- [W10 Pamięć wirtualna.]({{< ref "wyk/w10" >}})
- [W11 Systemy plików.]({{< ref "wyk/w11" >}})
- [W12 Realizacje systemów plików. Rozproszone systemy plików.]({{< ref "wyk/w12" >}})
- [W13 Systemy czasu rzeczywistego.]({{< ref "wyk/w13" >}})
- [W14 Ochrona i bezpieczeństwo.]({{< ref "wyk/w14" >}})
- [K1 Kolokwium 1.]({{< ref "wyk/k1" >}})
- [K2 Kolokwium 2.]({{< ref "wyk/k2" >}})

## Opisy ćwiczeń laboratoryjnych

- [L1 FIFO/pipe (3g)]({{< ref "lab/l1" >}})
- [L2 kolejki POSIX (3g)]({{< ref "lab/l2" >}})
- [L3 Gniazda pselect (3g)]({{< ref "lab/l3" >}})
- [L4  Gniazda, wątki, pamięć wspólna, synchronizacja (3g)]({{< ref "lab/l4" >}})

## Inne materiały

- [The Single UNIX specification, Version 3 (zawiera IEEE Std 1003.1 and ISO/IEC 9945)](http://www.unix.org/version3/online.html)
- POSIX [FAQ](http://www.opengroup.org/austin/papers/posix_faq.html)
		
