---
title: "Operating systems"
date: 2022-02-10T10:00:00+01:00
---

Resources for courses from Operating Systems series:

 - [Operating systems 1 (winter)]({{< ref sop1 >}})
 - [Operating systems 2 (summer)]({{< ref sop2 >}})
