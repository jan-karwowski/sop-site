---
title: "Asynchronous I/O"
date: 2022-02-05T22:35:56+01:00
weight: 70
---

# Lecture 8 - Asynchronous input/output

## Lecture Scope

1.  Benefits of Asynchronous I/O
2.  AIO control block structure
3.  Starting of AIO reads and writes
4.  AIO operation status
5.  Synchronization with AIO operations
6.  AIO canceling
7.  Examples

  
## References

1.  GNU Documentation: [here](http://www.gnu.org/software/libc/manual/html_node/Asynchronous-I_002fO.html)
2.  Lecture slides: [SOP1\_AIO.pdf]({{< resource "SOP1_AIO_0.pdf" >}})
