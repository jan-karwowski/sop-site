---
title: "Asynchroniczne I/O"
date: 2022-02-05T18:02:42+01:00
weight: 70
---

# Wykład 7 - Asynchroniczne operacje wejścia/wyjścia

## Zakres wykładu

1.  Omówienie korzyści z asynchronicznych operacji IO
2.  Blok kontrolny operacji IO
3.  Inicjowanie operacji asynchronicznego IO
4.  Sprawdzanie statusu operacji AIO
5.  Czekanie na zakończenie operacji AIO
6.  Anulowanie operacji AIO
7.  Przykłady

  
## Materiały

1.  Dokumentacja GNU: [tutaj](http://www.gnu.org/software/libc/manual/html_node/Asynchronous-I_002fO.html)
2.  Slajdy do wykładu: [SOP1\_AIO.pdf]({{< resource "SOP1_AIO_0.pdf" >}})
