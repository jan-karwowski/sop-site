---
title: "Scheduler"
date: 2022-02-05T22:35:50+01:00
weight: 80
---

# Lecture 8 - CPU Scheduling

## Scope

- Basic concepts - CPU-I/O burst cycles, statistics of burst cycles and scheduling.
- Short-term scheduler, preemptive vs non-preemptive scheduling, dispatcher, dispatch latency.
- Scheduling criteria.
- Basic scheduling algorithms: FCFS, SJF/SRTF, priority, round-robin (RR).
- Exponential average for next burst cycle time prediction.
- Gantt Diagram. Average and maximum waiting times. Turn-around time.
- Multilevel queues. Multilevel queues with feedback.
- Basics of Linux and MS Windows scheduling.

## Reference

1. Textbook: chapter "Process scheduling" - nr 5 in 8th and 10th ed., nr 6 in 9th ed.
2. Slides: [CPU\_scheduling.pdf]({{< resource "CPU_scheduling_6.pdf" >}})
