---
title: "Przydział CPU"
date: 2022-02-05T18:02:38+01:00
weight: 80
---

# Wykład 8 - Planowanie przydziału czasu CPU

## Zakres wykładu

- Pojęcia podstawowe: faza procesora, faza oczekiwania (wejścia/wyjścia). 
- Statystyki faz procesora i algorytm planowania przydziału.
- Planista przydziału procesora (krótkoterminowy), ekspedytor. Planowanie wywłaszczające i niewywłaszczające
- Kryteria planowania i optymalizacji algorytmu przydziału procesora.
- Podstawowe algorytmy szeregowania: FCFS, SJF/SRTF, priority, round-robin (RR).
- Uśrednianie wykładnicze a przewidywanie długości następnej fazy procesora.
- Diagram Gantta. Średni i maksymalny czas oczekiwania, czas obiegu.
- Kolejki wielopoziomowe i ze sprzężeniem zwrotnym.
- Podstawowe cechy algorytmów przydziału procesora w systemach Linux i MS Windows.

## Materiały

1. podręcznik: rozdział 6 (planowanie przydziału procesora).
2. slajdy: [Planowanie\_CPU.pdf]({{< resource "Planowanie_CPU_6.pdf" >}})
