---
title: "Introduction"
date: 2022-02-05T22:35:24+01:00
weight: 10
---

# Lecture 1 - Operating and computer systems

## Scope

- What is the operating system?
- Four components of the computer system.
- Fundamental modes of computer system operation: 
  - off-line,
  - on-line / interactive,
  - real-time (RT).

- Operating system goals.
- Computer system resources.
- Operating system services.
- System functions, system API, API standards.
- Operating systems structures: 
  - simple, monolithic kernel, 
  - mikrokernel, 
  - hybrid, 
  - use of loadable kernel modules.

- Interrupts, traps - handling and use (I/O, system function calls, error detection).
- DMA - application for fast CPU-I/O device communication; memory-related limitations.
- Hardware protection: dual mode CPU (and priviledged instructions), I/O protection, memory protection, system timer.

## Reference

1. Textbook (8th-10th ed.): chapter. 1 (Introduction), 2 (System structures).
2. Slides: [Intro.pdf]({{< resource Intro_2.pdf >}})
