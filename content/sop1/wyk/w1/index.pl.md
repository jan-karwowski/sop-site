---
title: "Wstęp"
date: 2022-02-05T18:00:18+01:00
weight: 10
---

# Wykład 1 - Systemy operacyjne i komputerowe

## Zakres materiału

- Co to jest i do czego służy system operacyjny?
- Cztery składniki systemu komputerowego.
- Podstawowe tryby przetwarzania:
  - wsadowy,
  - z podziałem czasu (TS) / interakcyjny, 
  - czasu rzeczywistego (RT).

- Zadania systemu operacyjnego.
- Zasoby systemu komputerowego.
- Podsystemy systemu operacyjnego.
- Funkcje systemowe, API systemu operacyjnego, standardy API.
- Struktury systemów operacyjnych: 
  - prosta, z monolitycznym jądrem, 
  - z mikrojądrem, 
  - hybrydowa; 
  - wykorzystanie ładowalnych modułów jądra.

- Przerwania, pułapki - ich obsługa i wykorzystanie (obsługa wejścia/wyjścia, wywołanie funkcji systemowych, obsługa błędów).
- DMA - wykorzystanie do komunikacji CPU-szybkie urządzenia; ograniczenia wydajności.
- Ochrona sprzętowa: dualny tryb CPU, ochrona wejścia/wyjścia, ochrona pamięci, czasomierz.

## Materiały

1. Podręcznik: rozdz. 1 (Wstęp), 2 (Struktury systemów komputerowych), 3 (Struktury systemów operacyjnych).
2. Slajdy: [Wstęp.pdf]({{< resource "Wstęp_2.pdf" >}})
