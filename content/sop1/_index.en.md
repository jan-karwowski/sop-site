---
title: "Operating systems 1"
date: 2022-02-10T10:00:00+01:00
---

# Operating systems 1

[Operating systems 1 page in USOS](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1120-IN000-ISA-0235)

## Staff

### Lecture

- Paweł Sobótka
  - office hours: Thursday 10:00am-11:00am, room 524, please contact me beforehand
  - email: pawel.sobotka@pw.edu.pl

### Laboratory
- Jan Karwowski *lab coordinator*
	- office hours: wtorek 11:00-12:00, room 526
	- email: jan.karwowski@pw.edu.pl
- Weronika Głuszczak
	- groups 1, 2
- Kaustav Sengupta
	- groups 3, 4
