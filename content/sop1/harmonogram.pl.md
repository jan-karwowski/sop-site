---
title: "Harmonogram"
date: 2022-02-10T10:00:00+01:00
---

## Wykłady

Czwartki, godz. 8:15 - 10:00, s. 328

| Data       | Tytuł                                                        |
|------------| ------------------------------------------------------------ |
| 13.10.2022 | [Wstęp. Systemy komputerowe i operacyjne.]({{< ref "wyk/w1" >}}) |
| 27.10.2022 | [Procesy]({{< ref "wyk/w2" >}})                              |
| 10.11.2022 | [System plików i jego interfejs. Strumieniowe wejście/wyjście.]({{< ref "wyk/w3" >}}) |
| 24.11.2022 | [Sygnały POSIX]({{< ref "wyk/w4" >}})                        |
| 08.12.2022 | [Niskopoziomowe operacje wejścia/wyjścia]({{< ref "wyk/w5" >}}) |
| 22.12.2022 | [Wątki. P-wątki i muteksy.]({{< ref "wyk/w6" >}})            |
| 12.01.2023 | [Asynchroniczne operacje wejścia/wyjścia]({{< ref "wyk/w7" >}}) |
| 26.01.2023 | [Planowanie przydziału czasu CPU]({{< ref "wyk/w8" >}})      |

## Zajęcia laboratoryjne

Wtorki, godz. 14:15 - 17:00, sale 218, 219, 304

| Data                      | Tytuł                                                          | Grupy Lab USOS  |
| ------------------------- | ---------                                                      | -------         |
| 2022-10-18                | [L0 Wprowadzenie]({{< ref "lab/l0" >}})                        | 1,3,5           |
| 2022-10-25                | [L0 Wprowadzenie]({{< ref "lab/l0" >}})                        | 2,4,6           |
| 2022-11-22                | [L1 Środowisko wykonania programu POSIX]({{< ref "lab/l1" >}}) | 1,3,5           |
| 2022-11-29                | [L1 Środowisko wykonania programu POSIX]({{< ref "lab/l1" >}}) | 2,4,6           |
| 2022-12-13                | [L2 Procesy i sygnały]({{< ref "lab/l2" >}})                   | 1,3,5           |
| 2022-12-20                | [L2 Procesy i sygnały]({{< ref "lab/l2" >}})                   | 2,4,6           |
| 2023-01-03                | [L3 Wątki i muteksy]({{< ref "lab/l3" >}})                     | 1,3,5           |
| 2023-01-10                | [L3 Wątki i muteksy]({{< ref "lab/l3" >}})                     | 2,4,6           |
| 2023-01-17                | [L4 AIO]({{< ref "lab/l4" >}})                                 | 1,3,5           |
| 2023-01-24                | [L4 AIO]({{< ref "lab/l4" >}})                                 | 2,4,6           |
| 2023-01-25                | **19:00** -- zamknięcie zapisów na poprawy w moodle            | Wszystkie grupy |
| 2023-01-30 9:00, 2023-02-01 9:00 | L5 Poprawy                                                     | Wszystkie grupy |


