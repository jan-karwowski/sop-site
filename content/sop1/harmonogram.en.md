---
title: "Schedule"
date: 2022-02-10T10:00:00+01:00
---

## Lecture

Thurdsays, 8:15 - 10:00, room 328

| Date       | Topic                                                        |
|------------| ------------------------------------------------------------ |
| 06.10.2022 | [Introduction. Computer and operating systems.]({{< ref "wyk/w1" >}}) |
| 20.10.2022 | [Processes]({{< ref "wyk/w2" >}})                            |
| 03.11.2022 | [File system and its interface. Stream-based I/O.]({{< ref "wyk/w3" >}}) |
| 17.11.2022 | [POSIX signals]({{< ref "wyk/w4" >}})                        |
| 01.12.2022 | [Low-level input/output operations.]({{< ref "wyk/w5" >}})   |
| 15.12.2022 | [Threads. P-threads and mutexes.]({{< ref "wyk/w6" >}})      |
| 04.01.2023 | [Asynchronous input/output.]({{< ref "wyk/w7" >}}) **Wednesday, room 102!** |
| 19.01.2023 | [CPU Scheduling]({{< ref "wyk/w8" >}})                       |

## Laboratories

Mondays, 2:15-5pm, rooms 203, 218.

| Date                | Topic                                                          | Lab group  in USOS |
| ----------          | -----------------------------------                            | ---------          |
| 2022-10-10          | [L0 Introduction]({{< ref "lab/l0" >}})                        | 1, 3               |
| 2022-10-17          | [L0 Introduction]({{< ref "lab/l0" >}})                        | 2, 4               |
| 2022-11-07          | [L1 POSIX program execution environment]({{< ref "lab/l1" >}}) | 1, 3               |
| 2022-11-14          | [L1 POSIX program execution environment]({{< ref "lab/l1" >}}) | 2, 4               |
| 2022-12-05          | [L2 Processes and signals]({{< ref "lab/l2" >}})               | 1, 3               |
| 2022-12-12          | [L2 Processes and signals]({{< ref "lab/l2" >}})               | 2, 4               |
| 2023-01-02          | [L3 Threads and mutexes]({{< ref "lab/l3" >}})                 | 1, 3               |
| 2023-01-09          | [L3 Threads and mutexes]({{< ref "lab/l3" >}})                 | 2, 4               |
| 2023-01-16          | [L4 AIO]({{< ref "lab/l4" >}})                                 | 1, 3               |
| 2023-01-23          | [L4 AIO]({{< ref "lab/l4" >}})                                 | 2, 4               |
| 2023-01-24          | **7:00pm** retake signup closes (in moodle)                    | Everyone           |
| 2023-02-09 8:30, 2023-02-09 11:15  | L5 Retakes                                                     | Everyone           |

