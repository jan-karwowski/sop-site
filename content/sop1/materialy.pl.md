---
title: "Materiały pomocnicze"
date: 2022-02-10T10:00:00+01:00
---

## Materiały wykładowe

- [W1 - Wstęp. Systemy komputerowe i operacyjne.]({{< ref "wyk/w1">}})
- [W2 - Procesy]({{< ref "wyk/w2">}})
- [W3 - Interfejs systemu plików. Strumieniowe wejście/wyjście]({{< ref "wyk/w3">}})
- [W4 - Procesy i sygnały POSIX]({{< ref "wyk/w4">}})
- [W5 - Niskopoziomowe operacje wejścia/wyjścia]({{< ref "wyk/w5">}})
- [W6 - Wątki. P-wątki i muteksy]({{< ref "wyk/w6">}})
- [W7 - Asynchroniczne operacje wejścia/wyjścia]({{< ref "wyk/w7">}})
- [W8 - Zarządzanie procesami i wątkami]({{< ref "wyk/w8">}})

## Opisy ćwiczeń laboratoryjnych

- ćwiczenie nieoceniane: [L0 - Wprowadzenie]({{< ref "lab/l0">}})
- ćwiczenia oceniane:
    - [L1 Środowisko wykonania programu POSIX]({{< ref "lab/l1">}})
    - [L2 - Procesy i sygnały]({{< ref "lab/l2">}})
    - [L3 - Wątki i muteksy]({{< ref "lab/l3">}})
    - [L4 - Asynchroniczne operacje wejścia/wyjścia (3g)]({{< ref "lab/l4">}})

## Inne materiały

- [The GNU C library manual](http://www.gnu.org/software/libc/manual/).
- [The Single UNIX specification, Version 3](http://www.unix.org/version3/online.html) (zawiera IEEE Std 1003.1 and
  ISO/IEC 9945)
- [POSIX FAQ](http://www.opengroup.org/austin/papers/posix_faq.html)
