---
title: "L1 - Środowisko wykonania programu POSIX"
date: 2022-02-05T18:39:22+01:00
weight: 20
---

# Tutorial 1 - Środowisko wykonania programu POSIX

{{< hint info >}}
Uwagi wstępne:
- To jest mega łatwy tutorial, ale za to długi, kolejne będą coraz trudniejsze i krótsze
- Szybkie przejrzenie tutoriala prawdopodobnie nic nie pomoże, należy samodzielnie uruchomić programy, sprawdzić jak
  działają, poczytać materiały dodatkowe takie jak strony man. W trakcie czytania sugeruję wykonywać ćwiczenia a na
  koniec przykładowe zadanie.
- Na żółtych polach podaję dodatkowe informacje, niebieskie zawierają pytania i ćwiczenia. Pod pytaniami znajdują się
  odpowiedzi, które staną się widoczne dopiero po kliknięciu. Proszę najpierw spróbować sobie odpowiedzieć na pytanie
  samemu a dopiero potem sprawdzać odpowiedź.
- Pełne kody do zajęć znajdują się w załącznikach na dole strony.
- Materiały i ćwiczenia są ułożone w pewną logiczną całość, czasem do wykonania ćwiczenia konieczny jest stan osiągnięty
  poprzednim ćwiczeniem dlatego zalecam wykonywanie ćwiczeń w miarę przyswajania materiału.
- Większość ćwiczeń wymaga użycia konsoli poleceń, zazwyczaj zakładam, że pracujemy w jednym i tym samym katalogu
  roboczym więc wszystkie potrzebne pliki są "pod ręką" tzn. nie ma potrzeby podawania ścieżek dostępu.
- Czasem podaję znak $ aby podkreślić, że chodzi o polecenie konsolowe, nie piszemy go jednak w konsoli np.: piszę "
  $make" w konsoli wpisujemy samo "make".
- To, co ćwiczymy wróci podczas kolejnych zajęć. Jeśli po zajęciach i teście coś nadal pozostaje niejasne proszę to
  poćwiczyć a jeśli trzeba dopytać się u prowadzących.
{{< /hint >}}

## Zadanie 1 - stdout

Cel: Napisać i skompilować za pomocą programu make najprostszy program używający standardowego strumienia wyjścia

Co student musi wiedzieć:
- man 3p stdin
- man 3p printf
- man stdlib.h
- man make
- [Tutorial na temat gcc i make]({{< resource "tutorial.gcc_make.txt" >}})

Absolutnie nie doradzam szukania stron man przez googla, bo często znajdują się stare dokumenty a czasem dokumentacje
innych systemów (HP-UX,solaris), które mogą się różnić od standardu i zawierać błędy. Najlepiej używać lokalnego systemu
man na stacjach w laboratoriach.

Dokumentacja systemowa man podzielona jest na sekcje, opis tych sekcji jest podany na stronie opisu komendy man.

Uwaga! Proszę szukać informacji głównie na stronach posix (3p), a nie na stronach dokumentacji implementacji Linuksowej 
(2 i 3), różnice mogą być bardzo duże, obowiązuje znajomość standardu przed znajomością implementacji

Gdzie znaleźć dokumentację samego polecenia man?
{{< expand "Odpowiedź" >}}
```shell
man man
``` 
{{< /expand >}}

Jeśli ćwiczysz na własnym systemie Linux, upewnij się, że masz zainstalowane strony z dokumentacją posix, jeśli nie to z
pomocą "Wójka Google" sprawdź jak doinstalować te strony i zrób to.

Czemu wpisanie man printf nie pomoże nam w zrozumieniu funkcji printf?
{{< expand "Odpowiedź" >}}
Ponieważ otwiera się pierwsza sekcja pomocy, a ta dotyczy poleceń powłoki, ponieważ istnieje polecenie printf to
dostajemy informacje na jego temat.
Należy podać numer sekcji: `man 3 printf` aby obejrzeć pomoc Linuksową lub lepiej `man 3p printf` aby obejrzeć opis
standardu.
{{< /expand >}}

<em>rozwiązanie <b>prog1.c</b>:</em>
{{< includecode "prog1.c">}}

Zwróćcie uwagę na konieczność zwrócenia kodu wyjścia w funkcji main, chociaż można zmienić int na void to kłóci się to z
wymogami co do wartości, jakie mają zwracać wszystkie procesy wg POSIX.

`\n` na końcu tekstu nie jest przypadkowe, bez tego po wykonaniu programu w konsoli kolejny tzw. znak zachęty (zazwyczaj
$) nie pojawi się w nowej linii. Dobrą praktyką w systemach UNIX jest kończenie strumieni tekstowych oraz plików tekstu
znakiem nowej linii. Drugi aspekt to chęć opróżnienia bufora standardowego strumienia - stdout i stderr są buforowane
liniami, będzie jeszcze o tym mowa.

Zamiast podawać zero jako wartość zwracaną w razie pełnego powodzenia programu lepiej korzystać ze zdefiniowanych
stałych: `EXIT_SUCCESS` i `EXIT_FAILURE`, znajdziecie je w man `stdlib.h`

Skąd wiadomo jakie pliki nagłówkowe trzeba włączyć?
{{< expand "Odpowiedź" >}} stdio.h z man 3 printf, stdlib.h dla stałych {{< /expand >}}

Skompiluj program poleceniem "make prog1", używasz w ten sposób domyślnego szablonu kompilacji programu GNU make. 
Uruchom program wynikowy. Czemu taki sposób kompilacji będzie dla nas nieprzydatny?
{{< expand "Odpowiedź" >}} Brak flagi -Wall, nie wiemy jakie warningi "czają" się w naszym kodzie.  {{< /expand >}}

<em>rozwiązanie <b>Makefile</b></em>
```makefile
all: prog1
prog1: prog1.c	
	gcc -Wall -fsanitize=address,undefined -o prog1 prog1.c
.PHONY: clean all
clean:
	rm prog1
```

Struktura pliku Makefile jest opisana w tutorialu podanym na początku ćwiczenia, najważniejsze dla nas to pamiętać o obowiązkowym tabulatorze przed instrukcją kompilacji, pierwszy cel w pliku jest jednocześnie domyślnym celem (make == make all w powyższym przykładzie), cele opisane jako PHONY nie są powiązane z fizycznie istniejącymi plikami.

Nazwa każdego celu w makefile (poza .PHONY) musi być powiązana z plikiem wynikowym kompilacji (lub linkowania), częstym błędem studentów jest zapominanie o konsekwentnej zmianie wszystkich nazw w pliku Makefile podczas kopiowania powyższego pliku do innych przykładów!

Jak za pomocą programu make i podanego Makefile usunąć stary plik wykonywalny?
{{< expand "Odpowiedź" >}}
```shell
make clean
```
{{< /expand >}}

Jak za pomocą programu make i podanego Makefile przeprowadzić kompilacje?
{{< expand "Odpowiedź" >}}
`make` lub `make prog1`
{{< /expand >}}

Jak przekierować wyjcie tego programu do pliku?
{{< expand "Odpowiedź" >}} 
```shell
./prog1 > plik.txt
```
{{< /expand >}}

Jak teraz wyświetlić ten plik?
{{< expand "Odpowiedź" >}} 
```shell
cat plik.txt
```
{{< /expand >}}

Zrób kopię programu prog1.c, niech się nazywa prog1b.c. Przerób plik Makefile tak aby kompilował ten nowy plik do binarki o nazwie prog1b, zwróć uwagę aby przy faktycznej kompilacji (a nie tylko w Makefile) była obecna flaga -Wall. Jeśli uda Ci się to za pierwszym razem to spróbuj sprowokować błąd celowo zmieniając niektóre prog1b z powrotem na prog1 w różnych konfiguracjach.

## Zadanie 2 - stdin, stderr

Cel: Rozwinąć prog1.c tak aby wypisywał na ekran powitanie dla imienia podanego z linii poleceń, imiona powyżej 20
znaków powinny generować błąd programu (natychmiastowe wyjście i komunikat)

Co student musi wiedzieć: 
- man 3p fscanf
- man 3p perror
- man 3p fprintf

Dla ułatwienia pisania dodajmy makro:
```c
#define ERR(source) (perror(source),\
fprintf(stderr, "%s:%d\n", __FILE__, __LINE__),\
exit(EXIT_FAILURE))
```

-Zwróć uwagę na makra `__FILE__` i `__LINE__`, pokazują lokalizację wywołania w pliku źródłowy, to cecha preprocesora C.
- Brak średnika po makro jest celowy.
- Komunikaty o błędach zawsze wypisujemy na stderr.
- W reakcji na większość błędów kończymy program, stąd exit.

<em>dodatkowy kod do <b>prog2.c</b></em>

```c
char name[22];
scanf("%21s", name);
if (strlen(name) > 20) ERR("Name too long");
printf("Hello %s\n", name);
```

Przerób Makefile z poprzedniego przykłady tak, aby można było skompilować ten program.

Uruchom i przetestuj program.

Czemu w kodzie pojawia się 21 jako rozmiar maksymalny w formatowaniu scanf (%21s)?
{{< expand "Odpowiedź" >}}
Czytając maksymalnie 20 znaków nie wiemy czy użytkownik podał dokładnie 20 czy może więcej,
czytając maksymalnie 21 wiemy o przekroczeniu limitu. 
{{< /expand >}}

Czemu deklarujemy 22 jako rozmiar tablicy na ciąg znaków skoro czytamy najwyżej 21 znaków?
{{< expand "Odpowiedź" >}}
W C ciągi znaków kończą się znakiem zera, scanf dopisze to zero za ostatnim znakiem, 
zatem każdy ciąg zawsze musi mieć ten dodatkowy znak doliczony do rozmiaru.
{{< /expand >}}

Jak można zmienić sposób wywołania tego programu tak aby komunikat o ewentualnym błędzie wykonania nie pojawił się na
ekranie?
{{< expand "Odpowiedź" >}}
Można przekierować stderr do /dev/null, np. tak: `./prog2 2>/dev/null`
{{< /expand >}}

Gdy podajemy zbyt długie imię pojawia się komunikat "Name too long: Success", czemu success? Zmienna errno nie jest
ustawiana bo to błąd w naszym kodzie a nie w wywołaniu funkcji bibliotecznej - makro jest napisane przyszłościowo pod
kątem błędów w funkcjach.

Gdy podamy podwójne imię np.: "Anna Maria" program wypisze tylko pierwsze, w scanf prosimy o jeden wyraz a nie całą
linię. Całe linie będziemy czytać za chwilę.

Program nie zaczyna się od zapytania użytkownika o imię tylko od razu oczekuje danych. Takie podejście jest bardzo
UNIXowe, takie programy lepiej nadają się do użycia w skryptach lub w przetwarzaniu danych z plików (następne zadanie).
Na drugim biegunie jest podejście interaktywne (bardziej w stylu Windows), z grzeczną prośbą o podanie imienia na
początku. Wybór stylu interakcji zależy od zastosowania, można połączyć oba jeśli rozpoznamy (f. isatty) czy program
działa interaktywnie na konsoli czy może jest to tzw. przetwarzanie wsadowe.

W prog2.c nie wykorzystano wartości zwracanej przez scanf. Niestety zbyt długi wyraz na stdin
(dla specyfikacji %s) nie jest traktowany jako błąd i scanf() zwraca 1 (tak jak i dla wyrazu krótszego od 21 znaków)

W programie występują tzw. "magic numbers" 20,21,22. Nie powinno się ich tworzyć to zły styl programowania, trudno potem
zmienić limit z 20 linii na 30 bez analizy kodu. Powinno się dodać stałą (#define MAXL 20) i wszystko od niej
wyprowadzić (MAXL+1 , MAXL+2)

Musimy wcisnąć enter, aby program mógł wykonać scanf, wynika to z liniowego buforowania terminala, natychmiastowe
czytanie znaków z klawiatury wymaga sporo pracy w tym usunięcie buforowania terminala (man 3p tcsetattr), dodania flagi
O_NONBLOCK i czytania bezpośrednio z deskryptora, a nie strumienia. Zazwyczaj łatwiej do tego użyć biblioteki (np.
ncurses). Nie będziemy się tym zajmować na SOP.

## Zadanie 3 - stdin cd..

Cel: Rozwinąć prog2.c tak aby wypisywał na ekran powitanie dla każdego ciągu imion (słów) podanego ze standardowego
wejścia. Program ma pobierać całe linie tekstu (do 20 znaków) i wypisywać na stdout. Operacja jest powtarzana aż do
napotkania końca strumienia (C-d). Linie dłuższe niż 20 znaków mają być skracane ale błąd nie ma być zgłaszany

Co student musi wiedzieć: 
- man 3p fgets

`C-d` (czyli w notacji Windowsowej Ctrl d) powoduje zamknięcie strumienia, zupełnie jakbyśmy wykonali na nim close. Gdy
strumień napotyka permanentny koniec danych to taki stan nazywamy EOF (end of file). Zwróć uwagę, że to nie to samo co
chwilowy brak danych gdy program czeka na "wolnego" względem programu człowieka aby ten coś wpisał, to są dwie różne
sytuacje które musisz rozróżniać!

`C-d` działa tylko po znaku nowej linii! Strumień tekstowy powinien kończyć się znakiem nowej linii.

`C-c` wysyła SIGINT do aktywnej grupy procesów - zazwyczaj pozwala zgrabnie zakończyć cały program

`C-z` zwiesza program ( wysyłając SIGSTOP), można potem poleceniem jobs obejrzeć listę takich wiszących programów i
przywrócić wybrany do życia piszac %N gdzie N to numer wiszącego procesu

`C-\` wysyła SIGQUIT, kończy program i generuje zrzut pamięci

`C-s` zamraża terminal, nie ma wpływu na proces o ile ten nie przepełni bufora, wtedy musi czekać. Mając nawyk
zapisywania pracy Ctrl S z Windows łatwo można sobie niechcący zawiesić terminal. Aby odwiesić terminal naciskamy C-q.

Powyższe skróty działają w powłoce bash której używamy w labach (też w kilku innych powłokach).

<em>kod do <b>prog3.c</b></em>
{{< includecode "prog3.c" >}}
<em>Nowy <b>Makefile</b> do kompilacji wielu programów (celów):</em>
```makefile
all: prog1 prog2 prog3
prog1: prog1.c
	gcc -Wall -fsanitize=address,undefined -o prog1 prog1.c
prog2: prog2.c
	gcc -Wall -fsanitize=address,undefined -o prog2 prog2.c
prog3: prog3.c
	gcc -Wall -fsanitize=address,undefined -o prog3 prog3.c
.PHONY: clean all
clean:
	rm prog1 prog2 prog3
```

Skompiluj za pomocą polecenia make i uruchom program, jak dla powyższego Makefile skompilować tylko jeden cel?
{{< expand "Odpowiedź" >}}
```shell
make prog3
```
{{< /expand >}}
 
Sprawdź jak się zachowa dla ciągów 20 i 21 znakowych. Czemu akurat tak?
{{< expand "Odpowiedź" >}}
Dla ciągu 21 znakowego w tablicy nie zmieści się znak nowej linii, 
fgets nie przepełnia buforu zatem obcina to co przekracza podany limit - w tym przypadki nową linię. 
{{< /expand >}}
 
Czemu w wywołaniu printf nie dodaliśmy znaku nowej linii na końcu a mimo to powitania wyświetlają się w oddzielnych liniach?
{{< expand "Odpowiedź" >}} 
fgets pobiera też \n o ile zmieści się w buforze, wypisując taki ciąg nie musimy już dodawać swojego `\n`
{{< /expand >}}

Czemu rozmiar bufora jest MAX_LINE+2?
{{< expand "Odpowiedź" >}}
Musi się zmieścić znak nowej linii i kończące łańcuch znakowy zero
{{< /expand >}}

Zwróć uwagę, że `fgets` pracuje na dowolnym strumieniu, nie tylko na `stdin`.

Ten program jest wolny od "magic numbers" i tak powinno być zawsze.

<em>plik testowy <b>dane.txt</b></em>
```
Alice 
Marry Ann
Juan Luis
Tom
```

Utwórz plik tekstowy z powyższą zawartością.

Jak skłonić nasz program aby pobrał dane z pliku a nie z klawiatury (na dwa sposoby)?
{{< expand "Odpowiedź 1" >}} 
przekierować plik na stdin: `./prog3 < dane.txt`
{{< /expand >}}
{{< expand "Odpowiedź 2" >}}
wykorzystać potok : `cat dane.txt | ./prog3`
{{< /expand >}}

## Zadanie 4 - parametry wywołania programu 1

Cel: Napisać program wyświetlający wszystkie parametry wywołania programu

Co student musi wiedzieć: 
- man 1 xargs

<em>kod do <b>prog4.c</b></em>
{{< includecode "prog4.c" >}}

<em>Pełny kod <b>Makefile</b></em>

```makefile
CC=gcc
CFLAGS=-Wall -fsanitize=address,undefined
LDFLAGS=-fsanitize=address,undefined
```

Aby używać szablonów kompilacji trzeba podać nazwę pliku wykonywalnego jako parametr make, w tym przykładzie będzie to "
$ make prog4". Tym razem kompilacja jest z pożądaną flagą -Wall gdyż podany Makefile modyfikuje standardowy szablon.

Aby parametrem uczynić dwa lub więcej słów trzeba ująć je w apostrofy lub wpisać znak \ przed każdą spacją (lub innym
tzw. białym znakiem).

Argument o numerze zero to zawsze nazwa programu!

Skompiluj i uruchom program, przetestuj dla różnych parametrów.

Jak za pomocą programu xargs przekształcić zawartość pliku dane.txt na argumenty wywołania naszego programu?
{{< expand "Odpowiedź 1" >}} 
każdy wyraz to oddzielny argument: `cat dane.txt | xargs ./prog4`
{{< /expand >}}
{{< expand "Odpowiedź 2" >}} 
każda linia to argument: `cat dane.txt |tr "\n" "\0"| xargs -0 ./prog4`
{{< /expand >}}

Używając powyższego dla większych plików trzeba pamiętać, że długość linii poleceń jest limitowana (różnie w różnych
systemach). `xargs` może podzielić dane na więcej wywołań gdy sobie tego zażyczymy.

## Zadanie 5 - parametry wywołania programu 2

Cel: Napisać program przyjmujący 2 argumenty: imię i licznik n>0, podanie złej ilości parametrów lub niepoprawnego
licznika ma zatrzymać program. Dla poprawnych argumentów program wypisuje "Hello IMIĘ"   n - raz

Co student musi wiedzieć: 
- man 3p exit
- man 3p atoi
- man 3p strtol

<em>Funkcja pomocnicza w pliku <b>prog5.c</b></em>
```c
void usage(char *pname)
{
  fprintf(stderr, "USAGE:%s name times>0\n", pname);
  exit(EXIT_FAILURE);
}
```

Zwróć uwagę, że funkcji exit możesz podawać takie same statusu jak zwracasz w main.

Dobrą praktyką jest wypisywanie tzw. usage przy braku wymaganych argumentów

<em>Kod do pliku <b>prog5.c</b>:</em>
{{< includecode "prog5.c" >}}

Skompiluj ten program używając uniwersalnego Makefile z poprzedniego zadania.

Jak działa program dla wartości powtórzeń niepoprawnie podanych, czemu tak?
{{< expand "Odpowiedź" >}}
Nic nie wypisze ponieważ atoi pod Linuksem zwraca zero jeśli nie można zamienić tekstu na liczbę.
{{< /expand >}}

Czemu argc ma być 3, mamy przecież 2 argumenty? 
{{< expand "Odpowiedź" >}}
`argc` to jest licznik elementów w tablicy argv, mieszczą się tam dwa argumenty i nazwa programu, w sumie 3 elementy
{{< /expand >}}

Zwróć uwagę na obrócony zapis porównania (0==j), jaki z takiego pisania może być zysk? Jeśli pomyłkowo zapiszę (0=j) to
się nie skompiluje i szybko dowiem się o błędzie. Gdybym napisał (j=0) to się skompiluje a o miejscu wystąpienia błędu
dowiem się dopiero po czasochłonnej analizie kodu. To jest dodatkowe zabezpieczenie.

Zwróć uwagę, że starsze standardy C nie dopuszczały deklarowania zmiennych w środku kodu, ale tak jest czytelniej bo
zmienne deklarujemy gdy stają się potrzebne.

Co zwraca atoi dla nie liczb? W praktyce zero wg. POSIX - undefined. Czasem chcemy mieć nad tym kontrolę, umieć
rozróżnić zero od błędu, wtedy używamy funkcji strtol.

Argumenty programu można nadpisać w trakcie jego działania , w tym i nazwę! Może to służyć próbie ukrycia procesu i/lub
jego argumentów (np. hasła).

## Zadanie 6 - parametry wywołania programu 3

Cel: Napisać program przyjmujący dowolną liczbę parametrów typu -t x i dowolną liczbę parametrów -n NAME. Parametry mogą
wystąpić w dowolnej kolejności. Dla każdego wystąpienia parametru -n wypisujemy dokładnie x razy powitanie "Hello NAME".
Początkowo x=1 ale kolejne wystąpienia -t mogą to zmienia

Np.: `./prog6 -n Anna -t 2 -n John -n Matt -t 1 -n Danny` wypisze
```
Hello Anna
Hello Jonh
Hello Jonh
Hello Matt
Hello Matt
Hello Danny
```

Co student musi wiedzieć: 
- man 3p getopt 
- [dokumentacja GNU](http://www.gnu.org/software/libc/manual/html_node/Using-Getopt.html)

<em>poprawmy funkcję usage w <b>prog6.c</b></em>
```c
fprintf(stderr, "USAGE:%s ([-t x] -n Name) ... \n", pname);
```
<em>kod do pliku <b>prog6.c</b></em>
{{< includecode "prog6.c" >}}

Sprawdź jak działa program dla niepoprawnych argumentów, lub gdy podamy argumenty pozycyjne (nie poprzedzone oznaczeniem
opcji) oraz gdy nie podamy argumentów lub parametrów.

W programie zmienną c (int) porównujemy ze znakami 't' i 'n' (char), to poprawne podejście w C char jest po prostu
jedno-bajtowym typem numerycznym.

W parametrach getopt opisujemy pożądaną składnię - zazwyczaj literki opcji z opcjonalnymi przyrostkami - `:` znaczy ma
argument, natomiast `::` znaczy, że ma opcjonalny argument.

Komunikaty dla błędnych opcji " invalid option -- '?' " są generowane w samej funkcji getopt jeśli chcemy je wyłączyć
ustawiamy zmienną globalną opterr na zero.

Zmienne optarg i optind to kolejne zmienne globalne typu extern z biblioteki powiązane z getopt. Co zawiera pierwsza
widać w kodzie, druga mówi ile opcji z linii wywołania do danego momentu przetworzono. Trzeba przy tym wiedzieć, że
argumenty nie będące opcjami lub ich argumentami są w trakcie przetwarzania przesuwane na koniec linii poleceń, teraz
już wiesz co robi ostatni warunek w kodzie powyżej - sprawdza czy nie było argumentów pozycyjnych z poza wymaganej
składni.

Zwróć uwagę, że lepiej byłoby najpierw przeprowadzić parsowanie opcji kontrolnie a dopiero potem wykonać program, ale
jako przykład tyle wystarczy.

`optarg` to statyczny bufor nadpisywany kolejnymi wywołaniami, czasem gdy musimy przechować te wartości trzeba je
kopiować (strcpy, alokacja pamięci).

Długie opcje np.: `--count 10` są obsługiwane przez `getopt_long` ale nie są one standaryzowane przez POSIX to
rozszerzenie GNU i nie będziemy ich używać.

## Zadanie 7 - zmienne środowiskowe 1

Cel: Napisać program wypisujący listę wszystkich zmiennych środowiskowyc

Co student musi wiedzieć: 
- man 3p environ
- man 7 environ

<em>zawartość do pliku <b>prog7.c</b></em>
{{< includecode "prog7.c" >}}

Własną zmienna mogę dodać np. tak: `TVAR2="122345" ./prog7` , pojawi się na wypisie ale nie zostanie zapamiętana w
powłoce, tzn. kolejne wywołania programu  `./prog7` już jej nie pokażą.

Mogę też dodać zmienną trwale do środowiska powłoki `export TVAR1='qwert'` i teraz ilekroć wywołam program `./prog7`
ta zmienna wciąż tam będzie.

Czy jeśli uruchomię drugą powłokę z menu środowiska i w niej uruchomię program to zmienna TVAR1 nadal będzie widoczna?
{{< expand "Odpowiedź" >}}
Nie, te dwie powłoki dziedziczą zmienne od procesu uruchamiającego programy w środowisku
zatem druga powłoka nie może dziedziczyć nic "w bok" od pierwszej.
{{< /expand >}}

Czy jeśli uruchomię drugą powłokę z pierwszej i w niej uruchomię program to zmienna TVAR1 nadal będzie widoczna?
{{< expand "Odpowiedź" >}}
Tak, druga powłoka dziedziczy zmienne od swojego rodzica czyli od pierwszej powłoki. 
{{< /expand >}}

## Zadanie 8 - zmienne środowiskowe 2

Cel: Zmodyfikować prog3.c tak aby każda linia tekstu "Hello NAME" była powielona tyle razy ile nakazuje mnożnik
przekazany przez zmienną środowiskową TIMES. Na zakończenie programu ustawić zmienną środowiskową RESULT na "Done

Co student musi wiedzieć: 
- man 3p getenv
- man 3p putenv
- man 3p setenv
- man 3 system (3p) jest trochę mniej czytelny

<em>kod do pliku <b>prog8.c</b></em>
{{< includecode "prog8.c" >}}

Zwróć uwagę na możliwy błąd braku zmiennej środowiska i jego obsługę na początku kodu. Dobry programista nigdy nie
pomija sprawdzania błędów, jeśli się śpieszysz i pomijasz to Twój kod jest gorszy - musisz być tego świadomy/a.
Najgorzej gdy programista pomija sprawdzanie błędów z niewiedzy lub ignorancji.

Drugi przykład sprawdzenia błędu jest po wywołaniu putenv, skoro tak często mamy to robić to przydałoby się makro, coś
na wzór ERR którego to już raz użyliśmy.

Innym sposobem na manipulowanie środowiskiem jest funkcja setenv.

Funkcja system służy nam do sprawdzenia tego co i tak wiemy, skoro nie było błędu putenv to nie spodziewamy się aby tu
był, jednak na potrzeby przykładu robimy takie sprawdzenie dwa razy, raz getenv("RESULT") a potem jeszcze system.

Użycie funkcji system jest równoważne z uruchomieniem powłoki jako procesu dziecka, komenda jest podana. Funkcja system
zwróci to co normalnie zwróciłby program, to co można w bash'u sprawdzić wywołaniem  "$ $?" (pierwszy dolar nie jest
częścią polecenia) czyli status zakończenia ostatniego polecenia/programu.

Co ciekawe niektóre systemy nie maja powłoki, gdy brak shella funkcja system nie działa! Dostajemy błąd 127.

Wykonaj program z różnymi ustawieniami TIMES

Jak po wykonaniu programu sprawdzić czy zmienna RESULT jest ustawiona? Będzie ?
{{< expand "Odpowiedź" >}}  
`env|grep RESULT` nie będzie ustawiona bo zmienne nie są propagowane wzwyż drzewa procesów. 
Zmienna była ustawiona tylko w programie i w powłoce w nim na chwilę wywołanej (f. system),
ale po zakończeniu tych procesów nie zachowała się. 
{{< /expand >}}

## Zadanie 9 - katalogi 1

Cel: Napisać program zliczający (pliki, linki, katalogi i inne obiekty) w katalogu roboczym (bez podkatalogów)

Co student musi wiedzieć: 
- man 3p fdopendir (tylko opis opendir)
- man 3p closedir
- man 3p readdir
- man 0p dirent.h
- man 3p fstatat (tylko opis stat i lstat)
- man 3p errno
- man 2 lstat (opis makr testujących typ obiektu)

<em>funkcja do pliku <b>prog9.c</b></em>
```c
void scan_dir()
{
DIR *dirp;
struct dirent *dp;
struct stat filestat;
int dirs = 0, files = 0, links = 0, other = 0;
if (NULL == (dirp = opendir(".")))
ERR("opendir");
do {
errno = 0;
if ((dp = readdir(dirp)) != NULL) {
if (lstat(dp->d_name, &filestat))
ERR("lstat");
if (S_ISDIR(filestat.st_mode))
dirs++;
else if (S_ISREG(filestat.st_mode))
files++;
else if (S_ISLNK(filestat.st_mode))
links++;
else
other++;
}
} while (dp != NULL);

	if (errno != 0)
		ERR("readdir");
	if (closedir(dirp))
		ERR("closedir");
	printf("Files: %d, Dirs: %d, Links: %d, Other: %d\n", files, dirs, links, other);

}
```

Uruchom ten program w katalogu w którym masz jakieś pliki, może być ten w którym wykonujesz ten tutorial, ważne aby nie było w nim katalogów, czy wyniki zgadzają się z tym czego oczekujemy tj. zero katalogów, trochę plików?
{{< expand "Odpowiedź" >}} 
Nie, są dwa katalogi, program policzył katalog "." i  "..", każdy katalog ma hard link na samego siebie "." i katalog rodzic ".." 
{{< /expand >}}

Jak utworzyć link symboliczny do testów ? 
{{< expand "Odpowiedź" >}} 
```shell
ln -s prog9.c prog_link.c
```
{{< /expand >}}

Jak różnią się stat i lstat? Czy jeśli w kodzie zmienić lstat na stat to zliczymy linki poprawnie ? 
{{< expand "Odpowiedź" >}} 
Nie, link zostanie potraktowany tak jak obiekt który wskazuje, to jest właśnie różnica pomiędzy tymi dwoma funkcjami.
{{< /expand >}}

Jakie pola zawiera struktura opisująca obiekt w systemie plików (dirent) wg. POSIX ? 
{{< expand "Odpowiedź" >}} 
Tylko numer inode i nazwę, resztę danych o pliku odczytujemy funkcjami lstat/stat
{{< /expand >}}

Jakie pola zawiera struktura opisująca obiekt w systemie plików (dirent) w Linuksie (man readdir) ? 
{{< expand "Odpowiedź" >}} 
Numer inode, nazwę  i 3 inne nie objęte standardem
{{< /expand >}}

Tam gdzie implementacja Linuksa odbiega od standardu trzymamy się zawsze standardów, to powoduje większą przenośność
naszego kodu pomiędzy różnymi Unix'ami.

Zwróć uwagę na sposób obsługi błędów funkcji systemowych, zazwyczaj robimy to tak:  if(fun()) ERR(), makro ERR było już
omawiane wcześniej. Wszystkie funkcje mogące sprawiać kłopoty należy sprawdzać. W praktyce prawie wszystkie funkcje
systemowe wymagają sprawdzania. Będę to powtarzał do znudzenia, niesprawdzanie błędów jest grzechem głównym
programistów.

Większość błędów jakie napotkamy będzie wymagać zakończenia programu, wyjątki omówimy w kolejnych tutorialach.

Zwróć uwagę na użycie katalogu "." w kodzie, nie musimy znać aktualnego katalogu roboczego, tak jest prościej.

Funkcja readdir jest dość specyficzna gdyż zwraca NULL zarówno jako oznaczenie końca katalogu jak i jako oznaczenie
błędu! Jak więc sobie z tym poradzić?

Używamy errno do rozpoznania błędu readdir, wartość errno musi być zerowana przed KAŻDYM wywołaniem readdir a test jego
wartości musi być wykonany zanim nastąpią wywołania funkcji systemowych i bibliotecznych, które mogłyby to errno
wyzerować. Zwróć uwagę, że errno = 0 jest ustawiane w pętli przed readdir a nie np. raz przed pętlą oraz na to, że w
razie zwrócenia NULL przez readdir sterowanie przechodzi jedynie przez dwa proste warunki zanim dojdzie do sprawdzenia
errno i rozpoznania błędu.

Czemu w ogóle zerujemy errno? Przecież funkcja readdir mogłaby robić to za nas? No właśnie "mogłaby", dokładnie tak
definiuje to standard, funkcje systemowe mogą zerować errno w razie poprawnego wykonania ale nie muszą.

Jeśli chcemy w warunkach logicznych w C dokonywać przypisań to powinniśmy ująć całe przypisanie w nawiasy, wartość
przypisywana będzie wtedy uznana za wartość wyrażenia w nawiasie. Robimy tak w przypadku wywołania readdir.

Dobrzy programiści zawsze zwalniają zasoby, w tym programie zasobem jest otwarty katalog. W Linuksie otwarty katalog
liczy się jak otwarty plik, proces może mieć limit otwartych deskryptorów co daje nam już dwa bardzo ważne argumenty aby
pamiętać o closedir. Trzecim powodem będzie sprawdzający kod nauczyciel :-).

## Zadanie 10 - katalogi 2

Cel: Bazując na funkcji z poprzedniego zadania napisać program, który będzie zliczał obiekty we wszystkich folderach
podanych jako parametry pozycyjne programu

Co student musi wiedzieć: 
- man 3p getcwd
- man 3p chdir

<em>kod do pliku <b>prog10.c</b></em>
{{< includecode "prog10.c" >}}

Sprawdź jak program zachowa się w przypadku nieistniejących katalogów, katalogów co do których nie masz prawa dostępu , czy poprawnie poradzi sobie ze ścieżkami względnymi podawanymi jako parametr a jak z bezwzględnymi.

Czemu program pobiera i zapamiętuje aktualny katalog roboczy?
{{< expand "Odpowiedź" >}}
Podawane programowi jako parametry ścieżki mogą być względne czyli zapisane względem początkowego położenia w drzewie
katalogów, program przed wywołaniem funkcji skanującej z poprzedniego zadania zmienia katalog roboczy tak aby "być" w
folderze skanowanym. Ta zmiana powoduje, że wszystkie inne ścieżki względne stają się niepoprawne (są prowadzone z
innego punktu drzewa niż program działa) dlatego po sprawdzeniu katalogu program musi wrócić do katalogu wyjściowego
skąd kolejna ścieżka względna ma sens.
{{< /expand >}}

Czy prawdziwe jest stwierdzenie, że program powinien "wrócić" do tego katalogu w którym był uruchomiony?
{{< expand "Odpowiedź" >}}
Nie, katalog roboczy to właściwość procesu, jeśli proces dziecko zmienia swój CWD to nie ma to wpływu na proces rodzic,
zatem nie ma obowiązku ani potrzeby wracać.
{{< /expand >}}

W tym programie nie wszystkie błędy muszą zakończyć się wyjściem, który można inaczej obsłużyć i jak?
{{< expand "Odpowiedź" >}}
`if(chdir(argv[i])) continue;` To oczywiście nieco uproszczone rozwiązanie, można by dodać jakiś komunikat.
{{< /expand >}}

Nigdy i pod żadnym pozorem nie pisz `printf(argv[i])`, jeśli ktoś poda jako katalog %d to jak to wyświetli `printf`?
To dotyczy nie tylko argumentów programu ale dowolnych ciągów znaków.

## Zadanie 11 - katalogi 3

Cel: Napisać program zliczający wystąpienia plików, katalogów, linków i innych typów dla całych poddrzew zaczynających
się w podanych jako parametry folderach.

Co student musi wiedzieć: 
- man 3p ftw
- man 3p nftw

<em>kod w pliku <b>prog11.c</b></em>
{{< includecode "prog11.c" >}}

Powtórz sobie jak działają wskazania na funkcje w C.

Sprawdź jak program sobie radzi z niedostępnymi i nieistniejącymi katalogami.

W jakim celu użyta jest flaga `FTW_PHYS`?
{{< expand "Odpowiedź" >}} 
Bez tej flagi nftw przechodzi przez linki symboliczne do wskazywanych obiektów, czyli nie może ich zliczać, 
analogicznie jak fstat 
{{< /expand >}}

Sprawdź jak inne flagi modyfikują zachowanie nftw.

Deklaracja `_XOPEN_SOURCE` jest na Linuksie niezbędna, inaczej nie widzi deklaracji funkcji nftw (ważna jest kolejność,
deklaracja przed include), funkcję ftw oznaczono już jako przestarzałą.

Zmienne globalne to "zło wcielone", zbyt łatwo ich użyć a przychodzi za to zapłacić przy analizowaniu cudzego kodu lub
podczas przenoszenia funkcji z jednego projektu do drugiego. Tworzą one niejawne zależności w kodzie. Tu niestety musimy
ich użyć, funkcja callback nftw nie pozwala nic przekazać na zewnątrz inaczej jak przez zmienną globalną. To jest
wyjątkowa sytuacja, używanie zmiennych globalnych poza wskazanymi koniecznymi przypadkami jest na labach zabronione!

Bardzo przydatna jest możliwość nałożenia limitu otwieranych przez nftw deskryptorów, co prawda może to uniemożliwić
przeskanowanie bardzo głębokiego drzewa katalogów (głębszego niż limit) ale pozwala to nam zarządzać zasobami które
mamy. W zakresie deskryptorów maksima systemowe pod Linuksem są nieokreślone, ale można procesy oddzielnie limitować na
poziomie administracji systemem.

## Zadanie 12 - operacje na plikach

Cel: Napisać program tworzący nowy plik o podanej parametrami nazwie (-n NAME), uprawnieniach (-p OCTAL ) i rozmiarze (
-s SIZE). Zawartość pliku ma się składać w około 10% z losowych znaków [A-Z], resztę pliku wypełniają zera (znaki o
kodzie zero, nie '0'). Jeśli podany plik już istnieje, należy go skasować.

Co student musi wiedzieć: 
- man 3p fopen
- man 3p fclose
- man 3p fseek
- man 3p rand
- man 3p unlink
- man 3p umask

Dokumentacja glibc dotycząca umask <a href="http://www.gnu.org/software/libc/manual/html_node/Setting-Permissions.html">link</a>

<em>kod do pliku <b>prog12.c</b></em>
{{< includecode "prog12.c" >}}

Jaką maskę bitową tworzy wyrażenie `~perms&0777` ? 
{{< expand "Odpowiedź" >}}
odwrotność wymaganych parametrem -p uprawnień przycięta do 9 bitów, 
jeśli nie rozumiesz jak to działa koniecznie powtórz sobie operacje bitowe w C.
{{< /expand >}}

Jak działa losowanie znaków ? 
{{< expand "Odpowiedź" >}}
W losowych miejscach wstawia kolejne znaki alfabetu od A do Z potem znowu A itd.
Wyrażenie 'A'+(i%('Z'-'A'+1)) powinno być zrozumiałe, jeśli nie poświęć mu więcej czasu takie losowania będą się jeszcze pojawiać.
{{< /expand >}}

Uruchom program kilka razy, pliki wynikowe wyświetl poleceniem cat i less sprawdź jakie mają rozmiary (ls -l), czy zawsze równe podanej w parametrach wartości? Z czego wynikają różnice dla małych rozmiarów -s a z czego dla dużych (> 64K) rozmiarów?
{{< expand "Odpowiedź" >}}
Prawie zawsze rozmiary są różne w obu przypadkach wynika to ze sposobu tworzenia pliku,
który jest na początku pusty a potem w losowych lokalizacjach wstawiane są znaki, 
nie zawsze będzie wylosowany znak na ostatniej pozycji. Losowanie podlega limitowi 2 bajtowego RAND_MAX, 
wiec w dużych plikach losowane są znaki na pozycjach do granicy RAND_MAX.
{{< /expand >}}

Przerób program tak, aby rozmiar zawsze był zgodny z założonym.

Czemu podczas sprawdzania błędu unlink jeden przypadek ignorujemy?
{{< expand "Odpowiedź" >}}
ENOENT oznacza brak pliku, jeśli plik o podanej nazwie nie istniał to nie możemy go skasować,
ale to nie przeszkadza programowi, w tym kontekście to nie jest błąd. 
Bez tego wyjątku moglibyśmy tylko nadpisywać istniejące pliki a nie tworzyć nowe.
{{< /expand >}}

Zwrócić uwagę na wyłączenie z main funkcji do tworzenia pliku, im więcej kodu tym ważniejszy podział na użyteczne funkcję. Przy okazji krótko omówmy cechy dobrej funkcji:
- robi jedną rzecz na raz (krótki kod)
- możliwie duży stopień generalizacji problemu (dodano procent jako parametr)
- wszystkie dane wejściowe dostaje przez parametry (nie używamy zmiennych globalnych)
- wyniki przekazuje przez parametry wskaźnikowe lub wartość zwracaną (w tym przypadku wynikiem jest plik) a nie przez zmienne globalne

W kodzie używamy specjalnych typów numerycznych `ssize_t`, `mode_t` zamiast int robimy to ze względu na zgodność typów z
prototypami funkcji systemowych.

Czemu w tym programie używamy umask? Otóż funkcja fopen nie pozwala ustawić uprawnień, a przez umask możemy okroić
uprawnienia jakie są nadawane domyślnie przez fopen, niskopoziomowe open daje nam nad uprawnieniami większą kontrolę.

Czemu zatem nie możemy dodać uprawnień "x"? Funkcja fopen domyślnie nadaje tylko prawa 0666 a nie pełne 0777, przez
bitowe odejmowanie nijak nam nie może wyjść ta brakująca część 0111.

Jak zwykle sprawdzamy wszystkie błędy, ale nie sprawdzamy statusu umask, czemu? Otóż umask nie zwraca błędów tylko starą
maskę.

Zmiana umask jest lokalna dla naszego procesu i nie ma wpływu na proces rodzicielski zatem nie musimy jej przywracać.

Parametr tekstowy -p został zmieniony na oktalne uprawnienia dzięki funkcji strtol, warto znać takie przydatne funkcje
aby potem nie wyważać otwartych drzwi i nie próbować pisać samemu oczywistych konwersji.

Pytanie czemu kasujemy plik skoro tryb otwarcia w+ nadpisuje plik? Jeśli plik o danej nazwie istniał to jego uprawnienia
są zachowywane a my przecież musimy nadać nasze, przy okazji jest to pretekst do ćwiczenia kasowania.

Tryb otwarcia pliku "b" nie ma w systemach POSIX-owych żadnego znaczenia, nie rozróżniamy dostępu na tekstowy i binarny,
jest tylko binarny.

W programie nie wypełniamy pliku zerami, dzieje się to automatycznie ponieważ gdy zapisujemy coś poza aktualnym końcem
pliku system automatycznie dopełnia lukę zerami. Co więcej, jeśli tych zer ciągiem jest sporo to nie zajmują one
sektorów dysku!

Jeśli wykonamy unlink na pliku już otwartym i używanym w innym programie to plik zniknie z filesystemu ale nadal
zainteresowane procesy będą mogły z niego korzystać. Gdy skończą plik zniknie na dobre.

Najlepiej w procesie wywołać srand dokładnie jeden raz z unikalnym ziarnem,w tym programie wystarczy czas podany w
sekundach.

## Zadanie 13 – buforowanie standardowego wyjścia

Ten temat ma więcej wspólnego z ogólnym programowaniem w C niż z systemami operacyjnymi, niemniej jednak wspominamy o nim, bowiem w poprzednich latach był częstym źródłem problemów.

<em>kod do pliku <b>prog13.c</b></em>
{{< includecode "prog13.c" >}}

Spróbuj uruchomić ten (bardzo prosty!) kod z terminala. Co widać na terminalu? 
{{< expand "Odpowiedź" >}} 
To czego się spodziewaliśmy: co sekundę pokazuje się liczba.
{{</ expand >}}

Spróbuj uruchomić kod ponownie, tym razem jednak przekierowując wyjście do pliku `./plik_wykonwyalny >
plik_z_wyjściem`. Następnie spróbuj otworzyć plik z wyjściem w trakcie działania programu, a potem zakończyć
działanie programu przez Ctrl+C i otworzyć plik jeszcze raz. Co widać tym razem? 
{{< expand "Odpowiedź" >}} Jeśli zrobimy te kroki wystarczająco szybko, plik okazuje się być pusty! To zjawisko wynika z
tego, że biblioteka standardowa wykrywa, że dane nie trafiają bezpośrednio do terminala, i dla wydajności buforuje je, 
zapisując je do pliku dopiero gdy zbierze się ich wystarczająco dużo. To oznacza, że dane nie są dostępne od razu,
a w razie nietypowego zakończenia programu (tak jak kiedy użyliśmy Ctrl+C) mogą wręcz zostać stracone. 
Oczywiście, jeśli damy programowi dojść do końca działania, to wszystkie dane zostaną zapisane do pliku (proszę spróbować!). Mechanizm buforowania można skonfigurować,
ale nie musimy tego robić, jak za chwilę zobaczymy. 
{{</ expand >}}

Spróbuj uruchomić kod podobnie, ponownie pozwalając wyjściu trafić do terminala (jak za pierwszym razem), ale spróbuj usunąć nową linię z argumentu `printf`: `printf("%d", i);`. Co widzimy tym razem?
{{< expand "Odpowiedź" >}}
Wbrew temu co powiedzieliśmy wcześniej, nie widać wyjścia mimo to, że tym razem dane trafiają bezpośrednio do terminala;
dzieje się natomiast to samo co w poprzednim kroku. Otóż biblioteka buforuje standardowe wyjście nawet jeśli dane
trafiają do terminala; jedyną różnicą jest to, że reaguje na znak nowej linii, wypisując wszystkie dane zebrane w
buforze. To ten mechanizm sprawił, że w pierwszym kroku nie wydarzyło się nic dziwnego. Właśnie dlatego czasami zdarza
się Państwu, że `printf` nie wypisuje nic na ekran; jeśli zapomnimy o znaku nowej linii, standardowa
biblioteka nic nie wypisze na ekran dopóki w innym wypisywanym stringu nie pojawi się taki znak, lub program się nie
zakończy poprawnie.
{{</ expand >}}

Spróbuj ponownie zrobić poprzednie trzy kroki, tym razem jednak wypisując dane do strumienia standardowego błędu: `fprintf(stderr, /* parametry wcześniej przekazywane do printf */);`. Co dzieje się tym razem? Żeby przekierować standardowy błąd do pliku, należy użyć `>2` zamiast `>`. 
{{< expand "Odpowiedź" >}}
Tym razem nic się nie buforuje i zgodnie z oczekiwaniami widzimy jedną cyfrę co sekundę. Standardowa biblioteka nie
buforuje standardowego błędu, bowiem często wykorzystuje się go do debugowania. 
{{</ expand >}}

Często możemy chcieć użyć `printf(...)` do debugowania, dodając wywołania tej funkcji w celu sprawdzenia
wartości zmiennych bądź czy wywołanie dochodzi do jakiegoś miejsca w naszym kodzie. W takich przypadkach należy zamiast
tej funkcji użyć `fprintf(stderr, ...)` i wypisywać do standardowego błędu. W przeciwnym przypadku może się
okazać, że nasze dane zostaną zbuforowane i zostaną wypisane później niż się spodziewamy, a w skrajnych przypadkach
wcale. Jeśli nie wiemy, do którego strumienia wypisywać, należy preferować standardowy błąd.
Przy pisaniu prawdziwych aplikacji konsolowych strumienia standardowego wyjścia używa się wyłącznie do wypisywania
rezultatów, a do czegokolwiek innego używa się standardowego błędu. Na przykład `grep` wypisze znalezione
wystąpienia na standardowe wyjście, ale ewentualne błędy przy otwarciu pliku trafią na standardowy błąd. Nawet nasze
makro `ERR` wypisuje błąd do strumienia standardowego błędu.

Wykonaj przykładowe <a href="{{< ref "../l1-example" >}}">ćwiczenie</a> z poprzednich lat. To zadanie szacuję na 60
minut, jeśli wyrobisz się w tym czasie to znaczy, że jesteś dobrze przygotowany/a do zajęć. Pamiętaj, że w aktualnym
układzie zajęć będzie dane zadnie na nieco ponad godzinę, więc trochę bardziej pracochłonne.

## Kody źródłowe z treści tutoriala
{{% codeattachments %}}
