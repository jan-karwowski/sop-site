---
title: "L4 - Asynchronous input/output"
date: 2022-02-07T19:57:36+01:00
weight: 50
---

# Tutorial 4 - Asynchronous input/output

<div class="dlaN">
<p>AIO topic is significantly shorter that previous topics, so is this tutorial. We expect more self studding from you,  please read lecture materials and man pages in addition to this page.</p>
<hr/ >
<h3>How to prepare for the labs?</h3>
<p>Although the topic of this lab is AIO, you should also expect other already practiced techniques like threads, signals and files!</p>  

<p>For entry test and the task please read all the materials listed below. You do not need to read all the content of the man pages listed, it is enough to know the purpose of the function, what parameters does it take (roughly, do not memorize all the parameters), if it has flags how they affect the outcome. Full man page invocations (execute it in the console) are listed. <em>Preparation references from previous tutorial are still obligatory!</em><ul>
<li>man 7 aio</li>
<li>man 3p aio_read</li>
<li>man 3p aio_write</li>
<li>man 3p aio_fsync</li>
<li>man 3p aio_error</li>
<li>man 3p aio_return</li>
<li>man 3p aio_suspend</li>
<li>man 3p aio_cancel</li>
<li>AIO requires librt during linking!!!</li>
<!--li> Examples of asynchronous AIO notifications from  IBM <a href="http://www.ibm.com/developerworks/linux/library/l-async/#N10345"> here </a></li-->
</ul></p>
</div>
<hr/ >
<p><em>AIO task</em></p>
<p>
The program processes a  <em>workfile</em>. The file is split into <em>n</em> (n is a parameter) blocks of equal size. The program must copy blocks at random positions A  of the file to random positions B (not exchanging A with B, after the copy there are two copies of the block at position A and B). During the process the content of shifted blocks must be reversed byte by byte. The program must use AIO, the random indexes of concurrently processed blocks can not be the same. The exchange operation must be conducted <em>k</em> times (k is a parameter), only 3 buffers can be used for AIO, the program must provide optimal shifting  algorithm (i.e. with as little waiting for IO as possible). On SIGINT the program cancels all the pending aio operations and exits.</p>

<p>
<em>solution <b>archunix5a.c</b>:</em>
{{< includecode "archunix5a.c" >}}
</p>
<p>The above task and its solution was prepared by Jerzy Bartuszek (UNIX course).</p>
<div class="dlaN"><ol>
<li>Inclrease of BLOCKS value will not improve the algorithm, it will use only 3 blocks at a time and the rest will be just a waste of memory. Decrease of this value below 3 will turn the program useless.</li>
<li>Algorithms based on random decisions are easier to test if you fix the random seed. Instead of srand(time(NULL))  use srand(1) for testing. It will result in rand generating the same sequences of numbers every time you run the program.</li>
<li>Block size calculation is based on the file size decreased by one "blocksize = (getfilelength(fd) - 1) / n;". This decrease is a caused by the rule that UNIX text file should always end with new line character. As we do not want to move this last character in the file (\n) we must decrease the size by one. If you wish to use binary files (without last \n) remove - 1 from  the calculation.</li>
<li>Call to function aio_fsync requires separate synchronization as regular aio_read and aio_write operations thus double suspend call in function syncdata - first suspend is for write operation, the second for disk synchronization.</li>
</ol></div>
<div class="cwiczenie"><ol>
<li>Start with the analysis how the code runs for one iteration (k=1) and then for multiple iterations (k>0).</li>
<li>In what way 3 buffers and 3 aiocb's are utilised? What is stored in 3 structures starting at curpos?<br>
{{< answer >}} Buffers (and structures) are used in cyclic way. To ease the calculation of next index in the array (with wrapping) macro SHIFT was introduced. The designation of buffers in a sequence starting at curpos at the beginning of the iteration is: (curpos) ready block  to write in this iteration, (curpos+1) block read and ready for processing in this iteration, (curpos+2) block for  read operation to be started in this iteration. {{< /answer >}}</li>
<li>Try to describe what happens concurrently in the main loop, draw a time line whit time unit equal to one iteration, for each buffer (1,2,3) and each time slot (assume k=4) write what is done with the buffer. Possible notations are read (R), write (W), processing (P) and nothing (-).  Please notice that the first read and the last write are done outside of the main loop, do not include them.<br>
{{< answer >}}0 1 2<br>
- P R<br>
R W P<br>
P R W<br>
W P -{{< /answer >}}</li>
<li>Why AIO synchronization (aio_suspend) is not done immediately after the operation, instead it is postponed till the end of the iteration?<br>
{{< answer >}} To achieve some concurrency of IO and buffer processing. The synchronization is done after the buffer reversing and in time before it, both IO and processing runs in parallel. {{< /answer >}}</li>
<li>Are indexes of read block A and write block B always different?<br>
{{< answer >}} No, for one iteration no check is made, for many iterations we only check if concurrent read and write blocks are at different positions but do notice that in one iteration we read the block we are going to process during next iteration. It may happen that the write position will be exactly the same as read position.  {{< /answer >}}</li>
<li>What is the meaning of  different indexes condition in this task?<br>
{{< answer >}} For this algorithm it is critical not to read and write the same block in parallel (in the same iteration) thus the condition on indexes. Without it, data in file may get corrupted. The problem does not affect one iteration as this is solved without real AIO. {{< /answer >}}</li>
<li>When selcting values for indexes the second value is randomly selected from narrower range, the maximum is decreased by one, why?<br>
{{< answer >}} This trick helps us to keep code shorter,  if indexes are equal you always can increase the second one because it was at most maximum minus one. Without it you must code the case when indexes are equal and maximal at the same time. {{< /answer >}}</li>
<li>Why in first iteration of the main loop we skip write and in the last one we skip read operation?<br>
{{< answer >}} In first iteration we do not have ready buffer to write in the last one we do not need another read because we will not write it anywhere, we are done with iterations. {{< /answer >}}</li>
<li>Is it necessary to use AIO for the first read and the last write in the program?</li>
{{< answer >}} No, those operations must be immediately synchronized - regular read and write can be used instead. {{< /answer >}}</li>
<li>When AIO cancellation takes place, is it correctly coded?<br>
{{< answer >}} It is initiated in cleanup function provide the process received the SIGINT signal (we know about it from global work variable). After the cancellation this code is not checking if the cancellation was fully successful (AIO_CANCELED) or fully failed (AIO_ALLDONE) or partially done (AIO_NOTCANCELED). In the last case the program should wait for the operations still in progress before it can release the buffers' memory. {{< /answer >}} <br>
Correct the code for exercise. </li>
<li>Convert all AIO operations to synchronous IO (change all aio_ calls to synchronous IO calls and get rid of aio_suspend). Do some testing for small blocks (10B) and large blocks (2MB). To create large random file you can run this "$dd bs=1024 count=200000 if=/dev/urandom of=testBin.txt". 
When AIO is faster, can it be slower that regular IO? To measure time you can use time command ($ man time).<br>
{{< answer >}} In my tests small blocks were processed in comparable times, this proves the Linux implementation of AIO to be quite fast, I expected it to be slower.  Processing of large blocks was more than two times faster with AIO. I tested for 100 iterations. {{< /answer >}}</li>
</ol></div>
<hr/>
<p class="cwiczenie">
As an exercise do <a href="{{< ref "../l4-example" >}}">this</a> task. It was used in previous years. It is 90 minutes task and if you can do it in this time it means you are well prepared for the lab. 
</p>
