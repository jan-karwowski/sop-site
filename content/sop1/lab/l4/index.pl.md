---
title: "L4 - Asynchroniczne wejście/wyjście"
date: 2022-02-05T19:27:11+01:00
weight: 50
---

# L4 - Asynchroniczne wejście/wyjście

<div class="dlaN">
<p>Temat AIO jest wyraźnie mniej obszerny od poprzednich tematów i ten tutorial jest też krótszy. Proszę zwrócić uwagę na aspekt bardziej samodzielnego przygotowania się do zajęć na podstawie stron dokumentacji man i wykładu.</p>
<hr/ >
<h3>Jak się przygotować do zajęć?</h3>
<p>Tematem przewodnim zajęć jest AIO ale samo zadanie będzie oparte także o elementy już wcześniej przez Państwa ćwiczone, na pewno będą wątki, sygnały i pliki.</p>

<p>Do wejściówki i zadania proszę zapoznać się z materiałami podanymi poniżej, nie czytamy całości stron man ale tylko tyle aby wiedzieć co dana funkcja robi, jakie z grubsza przyjmuję parametry i jeśli ma flagi to jak one zmieniają jej działanie. <em>Materiały przygotowawcze z poprzednich zajęć nadal obowiązują!</em>
Podane zostały całe polecenia man do wydania z konsoli:<ul>
<li>man 7 aio</li>
<li>man 3p aio_read</li>
<li>man 3p aio_write</li>
<li>man 3p aio_fsync</li>
<li>man 3p aio_error</li>
<li>man 3p aio_return</li>
<li>man 3p aio_suspend</li>
<li>man 3p aio_cancel</li>
<li>AIO wymaga librt podczas linkowania !!!</li>
<!--li> Przykłady asynchronicznej notyfikacji AIO od IBM <a href="http://www.ibm.com/developerworks/linux/library/l-async/#N10345"> tutaj </a></li-->
</ul></p>
<!--p><b>Rozkład punktów z tego laboratorium to 8 za wejściówkę, 17 za postępy wykonania zadania i do -10 punktów za poprawność i styl kodowania.</b> <br/ >W szczególności będzie ujemnie punktowane:<ul>
<li>niesprawdzanie błędów funkcji systemowych i bibliotecznych</li>
<li>niezwalnianie zasobów systemowych</li>
<li>funkcje dłuższe niż 40 linii</li>
<li>nieuzasadnione zmienne globalne</li>
<li>użycie niebezpiecznych funkcji takich jak np.: sprintf i gets (przepełnienie bufora)</li>
<li>tam gdzie wystąpi obsługa sygnału dodatkowo:<ul>
  <li>brak restartowania funkcji przerwanych przed wykonaniem (błąd EINTR)</li>
  <li>brak wznawiania funkcji przerwanych w trakcie (sleep, nanosleep, read. write)</li>
  <li>umieszczanie logiki programu w funkcji obsługi sygnału, w szczególności jeśli ta logika na dłużej blokuje główny program</li>
</ul></li>
</ul></p-->
</div>
<hr/ >
<p><em>Zadanie AIO</em></p>

<p>Napisz program, który przetwarza plik <em>workfile</em>. Plik jest dzielony
	   na <em>n</em> bloków równej długości. Zadaniem aplikacji jest skopiowanie
	   zawartości losowo wybranego bloku A i zastąpienie nim innego, również losowo
	   wybranego bloku B w tym pliku odwracając jednocześnie kolejność znaków w
     buforze. Nie zamieniamy bloków A i B, kopia A trafia na pozycję bloku B. Należy wykorzystać operacje asynchronicznego I/O. Wylosowane indeksy
     bloków w trakcie jednej iteracji muszą być różne.
     Powyższa operacja ma zostać wykonana <em>k</em> razy. W zadaniu można
     wykorzystać maksymalnie trzy bufory, należy zapewnić optymalne
     wykorzystanie operacji asynchronicznych. Program ma reagować na sygnał
     SIGINT kończąc działanie i próbując przerwać przy tym wykonywanie
     wszystkich operacji asynchronicznych.</p>

<p>
<em>rozwiązanie <b>archunix5a.c</b>:</em>
{{< includecode "archunix5a.c" >}}

<p>Powyższe zadanie wraz z rozwiązaniem przygotował Jerzy Bartuszek.</p>
<div class="dlaN"><ol>
<li>Zmiana wartości BLOCKS na większą od 3 nic nie da, dodatkowe bloki nie zwiększą równoległości za to będziemy marnować pamięć , zmiana na wartość mniejszą od 3 uczyni program niepoprawnym.</li>
<li>Programy oparte na losowości można łatwiej testować i debugować jeśli losowy generator liczb zainicjujemy stałą znaną nam wartością, zamiast wywołania srand(time(NULL)) proponuję  na czas testów zmienić na srand(1). Spowoduje to losowanie zawsze tych samych sekwencji wartości w kolejnych uruchomieniach programu.</li>
<li>Obliczanie wielkości bloku bazuje na rozmiarze pliku pomniejszonym o jeden "blocksize = (getfilelength(fd) - 1) / n;" to pomniejszenie wynika z chęci pozostawienia na końcu (poza blokami) znaku nowej linii, który zwyczajowo jest obecny w plikach tekstowych pod UNIXem. Jeśli zdecydujemy się na użycie plików bez tego znaku trzeba to obliczenie zmodyfikować usuwając " - 1"</li>
<li>Operacja aio_fsync wymaga oddzielnego synchronizowania, stąd podwójna synchronizacja w funkcji syncdata, pierwsza dotyczy trwającej operacji zapisu, druga operacji synchronizowania z dyskiem.</li>
</ol></div>
<div class="cwiczenie"><ol>
<li>Na początek prześledź co się dzieje w programie dla pojedynczej iteracji (k=1), potem dla wielu iteracji (k>1).</li>
<li>Jaki jest schemat użycia 3 buforów i 3 struktur aiocb? Jakie dane są w kolejnych trzech strukturach począwszy od curpos?<br>
{{< answer >}} Bufory są używane cyklicznie, dla ułatwienia wyliczeń pozycji z "zawijaniem" używane jest makro SHIFT. Zawartość kolejnych struktur na początku iteracji to: (curpos) dane gotowe do zapisu, (curpos+1) dane odczytane i gotowe do przetworzenia, (curpos+2) pozycja na kolejny odczyt. {{< /answer >}}</li>
<li>Zwróć uwagę na to co w tym programie odbywa się równolegle, narysuj sobie oś czasu gdzie jednostką jest iteracja głównej pętli programu, w każdym kroku (załóż k=4) zaznacz co się dzieje w danym buforze (0,1,2), możliwe operacje to odczyt R, zapis W, przetwarzanie P i nic -. Pamiętamy,że pierwszy odczyt i ostatni zapis w programie odbywają się poza pętlą.<br>
Ad.<br>
{{< answer >}}0 1 2<br>
- P R<br>
R W P<br>
P R W<br>
W P -{{< /answer >}}</li>
<li>Czemu synchronizacja odczytu i zapisu nie jest wykonana od razu po zleceniu operacji a dopiero na koniec iteracji?<br>
{{< answer >}} Aby uzyskać równoległość operacji IO z przetwarzaniem, synchronizacja odbywa się dopiero po zakończeniu obracania bufora co daje szansę tym operacjom na wykonanie się w tle obliczeń. {{< /answer >}}</li>
<li>Czy w iteracji  indeksy czytanego bloku i zapisywanego są różne?<br>
{{< answer >}} Nie, dla jednej iteracji (k=1) nie są sprawdzane, dla wielu iteracji sprawdzamy jedynie czy wylosowane wartości są różne ale są to indeksy bloku do zapisu i kolejnego odczytu - ten odczyt nie dotyczy aktualnej iteracji a dopiero następnej {{< /answer >}}</li>
<li>Czemu zatem w zadaniu jest warunek o różnych indeksach w iteracji?<br>
{{< answer >}} Bez tego warunku, w jednym czasie moglibyśmy asynchronicznie zapisać blok na danej pozycji i asynchronicznie czytać blok z tej samej pozycji a to może doprowadzić do zniekształcenia zawartości bloku. Problem nie dotyczy pojedynczej iteracji (k=1), gdyż taki przypadek odbywa się synchronicznie.  {{< /answer >}}</li>
<li>Czemu w drugim losowaniu w getindex maksymalna wartość jest o jeden mniejsza od pierwszego losowania?<br>
{{< answer >}} Ten trik upraszcza kod, bez tego musielibyśmy dodatkowo sprawdzać przypadek gdy indeksy są równe i jednocześnie maksymalne, wtedy zamiast zwiększać musielibyśmy zmniejszać. {{< /answer >}}</li>
<li>Czemu w pierwszej iteracji głównej pętli pomijamy zapis a w ostatniej odczyt?<br>
{{< answer >}} W pierwszej iteracji nie mamy jeszcze danych do zapisu, w ostatniej nie potrzebujemy już czytać kolejnego bloku, przetwarzamy już ostatni. {{< /answer >}}</li>
<li>Czy pierwszy odczyt i ostatni zapis w programie muszą być wykonane za pomocą operacji AIO?</li>
{{< answer >}} Nie, ponieważ są od razu synchronizowane za pomocą aio_suspend to w łatwiej byłoby użyć zwykłego read i write. {{< /answer >}}</li>
<li>Kiedy następuje anulowanie operacji AIO i czy jest poprawnie przeprowadzone?<br>
{{< answer >}} Anulowanie nastąpi w funkcji cleanup o ile program otrzyma SIGINT (zmienna globalna work), po anulowaniu operacji nie ma pewności (nie jest to w programie sprawdzane) czy wszystkie operacje się zakończyły anulowaniem (AIO_CANCELED) lub sukcesem (AIO_ALLDONE), Jeśli jakieś operacje nadal trwają (AIO_NOTCANCELED) to trzeba na nie poczekać zanim przejdzie się do usuwania buforów. {{< /answer >}} <br>Poprawienie tego mechanizmu proszę potraktować jako dodatkowe ćwiczenie.</li>
<li>Przerób program tak aby zamiast operacji AIO wykonywał operacje synchronicznie (wystarczy zamienić operacje aio_ na ich odpowiedniki synchroniczne oraz pozbyć się aio_suspend). Wykonaj testy dla małych (10B) i dużych (2MB) rozmiarów bloków . Duży plik testowy (200MB) możesz zrobić poleceniem "$dd bs=1024 count=200000 if=/dev/urandom of=testBin.txt". Kiedy AIO jest szybsze a kiedy wolniejsze? Do pomiaru czasu działania możesz użyć polecenia time ($ man time).<br>
{{< answer >}} W moich testach małe bloki wypadły porównywalnie w obu przypadkach co jest oznacz, że AIO w implementacji Linuksowej jest mało obciążające. Dla dużych bloków różnice były już bardzo wyraźne na korzyść AIO - ponad 2 razy szybciej dla 100 operacji. {{< /answer >}}</li>
</ol></div>
<hr/>
<p class="cwiczenie">
Wykonaj przykładowe <a href="{{< ref "../l4-example" >}}">ćwiczenie</a> z poprzednich lat. To zadanie szacuję na 90 minut, jeśli wyrobisz się w tym czasie to znaczy, że jesteś dobrze przygotowany/a  do zajęć.  Możesz się spodziewać zadania o zbliżonej pracochłonności.
</p>

<h2>Kody źródłowe z treści tutoriala</h2>
{{% codeattachments %}}
