---
title: "Regulamin"
date: 2022-02-10T10:00:00+01:00
draft: false
layout: single
menu: sop1
---

Regulamin przedmiotu znajduje się [na stronie przemiotu w usosie](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&kod=1120-IN000-ISP-0236
), dostępny po zalogowaniu.

Pole "Regulamin przedmiotu".
