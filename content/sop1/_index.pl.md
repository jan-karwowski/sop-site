---
title: "Systemy operacyjne 1"
date: 2022-02-10T10:00:00+01:00
---

# Systemy operacyjne 1

[Strona przedmiotu Systemy operacyjne 1 w USOSie](https://usosweb.usos.pw.edu.pl/kontroler.php?_action=katalog2/przedmioty/pokazPrzedmiot&prz_kod=1120-IN000-ISP-0236)

## Obsada

### Wykład

- Paweł Sobótka
  - konsultacje: czwartek 10:00-11:00, pok 524, po uprzednim umówieniu
  - email: pawel.sobotka@pw.edu.pl

### Laboratorium

- Jan Karwowski *koordynator*
	- grupy lab 3, 4
	- konsultacje: wtorek 11:00-12:00, pok. 526
	- email: jan.karwowski@pw.edu.pl
- Tomasz Karwowski
	- grupy lab 5, 6
- Mateusz Majewski
	- grupy lab 1, 2
