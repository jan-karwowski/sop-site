---
title: "Reference material"
date: 2022-02-10T10:00:00+01:00
---

## Lecture reference

- [Lec1 - Introduction. Computer and operating systems.]({{< ref "wyk/w1" >}})
- [Lec2 - Processes.]({{< ref "wyk/w2" >}})
- [Lec3 - File system and its interface. Stream-based I/O.]({{< ref "wyk/w3" >}})
- [Lec4 - POSIX signals.]({{< ref "wyk/w4" >}})
- [Lec5 - Low-level input/output operations.]({{< ref "wyk/w5" >}})
- [Lec6 - Threads. P-threads and mutexes.]({{< ref "wyk/w6" >}})
- [Lec7 - Asynchronous input/output.]({{< ref "wyk/w7" >}})
- [Lec8 - Management of threads and processes.]({{< ref "wyk/w8" >}})

## Laboratory reference

- not graded: [Lab0 - Introduction]({{< ref "lab/l0" >}})
- graded:
    - [Lab1 - POSIX program execution environment]({{< ref "lab/l1" >}})
    - [Lab2 - Processes and signals]({{< ref "lab/l2" >}})
    - [Lab3 - Threads and mutexes]({{< ref "lab/l3" >}})
    - [Lab4 - AIO]({{< ref "lab/l4" >}})

## Other

- [The GNU C library manual](http://www.gnu.org/software/libc/manual/).
- [The Single UNIX specification](http://www.unix.org/version3/online.html), Version 3 (includes IEEE Std 1003.1 and
  ISO/IEC 9945)
- [POSIX FAQ](http://www.opengroup.org/austin/papers/posix_faq.html)
